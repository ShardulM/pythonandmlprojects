import itertools
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
import pandas as pd
import matplotlib.ticker as ticker
from sklearn import preprocessing

#read
df = pd.read_csv('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/ML0120ENv3/Dataset/ML0101EN_EDX_skill_up/cbb.csv')

#creatin win index
df['windex'] = np.where(df.WAB > 7, 'True', 'False')
df['extra'] = 777
df1 = df.loc[df['POSTSEASON'].str.contains('F4|S16|E8', na=False)]
df1['windex'].replace(to_replace=['False','True'], value=[0,1],inplace=True)

print('\n','Appended dataset','\n','\n', df1.head())

#Feature selection
X = df1[['G', 'W', 'ADJOE', 'ADJDE', 'BARTHAG', 'EFG_O', 'EFG_D','TOR', 'TORD', 'ORB', 'DRB', 'FTR', 'FTRD', '2P_O', '2P_D', '3P_O','3P_D', 'ADJ_T', 'WAB', 'SEED', 'windex']]

y = df1['POSTSEASON'].values

#Normalizing
X= preprocessing.StandardScaler().fit(X).transform(X)

#Train validation split
from sklearn.model_selection import train_test_split
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=4)

#Using KNN as the algorithm   

from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier

#Finding the best k to fit the model

mean_acc = np.zeros((15))
for n in range(1,16):

    neigh2 = KNeighborsClassifier(n_neighbors = n).fit(X_train,y_train)
    yhat=neigh2.predict(X_val)
    mean_acc[n-1] = accuracy_score(y_val, yhat)

print('Test set accuracy for k = [1,16]:', mean_acc)

#best k are 5 and 8

#with k=5 
k = 5 
neigh = KNeighborsClassifier(n_neighbors = k).fit(X_train,y_train)
#prediction model:
yhat = neigh.predict(X_val)

#Using random forest algorithm
from sklearn.tree import DecisionTreeClassifier


Dectree = []
for i in range(1, 15):
    BB2tree = DecisionTreeClassifier(criterion="entropy", max_depth = i)
    BB2tree.fit(X_train,y_train)
    predTree = BB2tree.predict(X_val)
    Dectree.append(accuracy_score(y_val, predTree))

z = max(Dectree)
best = np.where(Dectree == z)
ans = min(best[0] + 1)
print('best max_depth = ', ans, 'which is: ', z)

#best max depth=1

BB2tree2 = DecisionTreeClassifier(criterion="entropy", max_depth = 1)
BB2tree2.fit(X_train,y_train)
#prediction model:
pred_Tree = BB2tree2.predict(X_val)

#using svm
from sklearn import svm

kernels = ['linear', 'poly', 'rbf', 'sigmoid']
bst = []
for i in kernels:
    clf = svm.SVC(kernel=i)
    clf.fit(X_train, y_train) 
    ypred = clf.predict(X_val)
    #print("SVM Accuracy for: ",i ,'is ', accuracy_score(y_val, ypred))
    bst.append(accuracy_score(y_val, ypred))
answer= np.where(kernels == max(bst))
print('The best kernal is: ', answer)

#best kernel is poly

clf2 = svm.SVC(kernel='poly')
clf2.fit(X_train, y_train) 
#prediction model:
y_pred = clf2.predict(X_val)

#Test set:
test_df = pd.read_csv('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/ML0120ENv3/Dataset/ML0101EN_EDX_skill_up/basketball_train.csv',error_bad_lines=False)

#Creating windex and normalizing data
test_df['windex'] = np.where(test_df.WAB > 7, 'True', 'False')
test_df1 = test_df[test_df['POSTSEASON'].str.contains('F4|S16|E8', na=False)]
test_Feature = test_df1[['G', 'W', 'ADJOE', 'ADJDE', 'BARTHAG', 'EFG_O', 'EFG_D', 'TOR', 'TORD', 'ORB', 'DRB', 'FTR', 'FTRD', '2P_O', '2P_D', '3P_O', '3P_D', 'ADJ_T', 'WAB', 'SEED', 'windex']]
test_Feature['windex'].replace(to_replace=['False','True'], value=[0,1],inplace=True)
test_X=test_Feature
test_X= preprocessing.StandardScaler().fit(test_X).transform(test_X)
test_y = test_df1['POSTSEASON'].values

#Evaluating models with f1 score
from sklearn.metrics import f1_score

#KNN
y_KNN = neigh.predict(test_X)

#Random forest
y_rf = BB2tree2.predict(test_X)

#SVM
y_SVM = clf2.predict(test_X)

#Finding the score
print('KNN: ', f1_score(test_y, y_KNN, average='micro'))

print('Random Forest: ', f1_score(test_y, y_rf, average='micro'))

print('SVM: ', f1_score(test_y, y_SVM, average='micro'))

#Final Prediction graphically

ds = test_df1[0:13]
real  = ds['POSTSEASON'].values
college_list = ds['TEAM'].values

KNNpred = y_KNN[0:13]
RFpred = y_rf[0:13]
SVMpred = y_SVM[0:13]


fig, (ax1, ax2, ax3) = plt.subplots(3,1)
ax1.plot(college_list, real, label="Real")
ax1.plot(college_list, KNNpred, label="KNN Prediction")
ax1.set_ylabel('KNN')

ax2.plot(college_list, real, label="Real")
ax2.plot(college_list, RFpred, label="Random FOrest Prediction")
ax2.set_ylabel('Random Forest')

ax3.plot(college_list, real, label="Real")
ax3.plot(college_list, SVMpred, label="SVM Prediction")
ax3.set_ylabel('SVM')

plt.show()
