import sys
import mpmath
sys.modules['sympy.mpmath'] = mpmath
import sympy as sym
from sympy import sympify
from mpmath import *
mp.dps = 15; mp.pretty = True
import numpy as np
import math
import matplotlib.pyplot as plt

x1 = sym.Symbol("x")

print('The taylor series creates an equation that approximates a function at a specific value')

def taylor_function(input_function, series_length, value):
    initial_list = []
    initial_list2 = []
    init_list = []
    init_list2 = []
    intermediate_list = []
    final_list = []

    def fact(n):
        #returns the factorial
        var = math.factorial(n)
        return round(var, 10)

    def removezero(initial_list2):
        #removes uneccesary zeroes
        for i in range(len(initial_list2)):
            try:
                if initial_list2[i] == 0:
                    initial_list2.pop(i)
            except:
                continue 
        try:
            initial_list2[0] = round(initial_list2[0], 3)
        finally:    
            return initial_list2

    for i in range(0, series_length):
        #crating the coefficient term
        temp = diff(input_function, value, i)
        if temp < 1.2e-34 and temp> -1.2e-34:
            temp = 0
        else:
            round(temp, 20)  
        coef = temp/fact(i)
        init_list.append(round(coef, 20))

    x2 = np.linspace(-7, 7, 256)
    x = x2 - value

    for i in range(0, series_length):
        #adding first constant
        k = init_list[i]*(x2**i)
        init_list2.append(k)  

    y = sum(init_list2)
 
    for i in range(0, series_length):
        #finding all differential
        #temp = diff(lambda x: input_function(x), 0, i)
        temp = diff(input_function, value, i)
        initial_list.append(temp)
   
    for i in range(len(initial_list)):
        #removes the very small numbers
        if initial_list[i] < 1.2e-34 and initial_list[i] > -1.2e-34:
            initial_list[i] = 0

    def show_term2(initial_list2, initial_list, x1):
        #adds the individual terms
        for i in range(len(initial_list)):
            numb = initial_list[i]*1/fact(i)
            round_num = round(numb, 20)
            lt = round_num*((x1 - value)**i)
            initial_list2.append(lt)
        return initial_list2
    
    def term_adder(lname):
        #returns a series which is a sum of terms bfore
        finallist = []
        for i in range(len(lname)):
            if i == 0:
                finallist.append(lname[0])
            else:
                finallist.append(lname[i] + finallist[i - 1])
        return finallist
    
    #creating final_list
    intermediate_list = term_adder(removezero(show_term2(initial_list2, initial_list, x1)))
    for i in intermediate_list:
        final_list.append(i)
   
    try:
        plt.plot(x, y, '-g', label= final_list[-1])
    except IndexError:
        plt.plot(x, y, '-g')

    axes = plt.gca()
    axes.set_xlim([x.min(), x.max()])
    axes.set_ylim([y.min(), y.max()])

    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Modifying curves')
    plt.legend(loc='upper left')

    plt.show()

    try:
        print(final_list[-1])
    except:
        pass


print('The equation that creates the series: '," f(x) = f(a)+f'(a)*(x-a) + (f''(a))*(x-a)^2/(2!) + (f^((3))(a))*(x-a)^3/(3!) +...+ (f^((n))(a))*(x-a)^n/(n!)+....")    

def calling():
    global sin_or_cos
    sin_or_cos = input('enter sin, cos or e to see its taylor expansion: ')
    val  = int(input('whach value do you want to start at: '))

    if sin_or_cos == 'sin':
        x = np.linspace(-7, 7, 256, endpoint = True)
        y = np.sin(x)

        plt.plot(x, y, '-g', label=r'$sin$')

        axes = plt.gca()
        axes.set_xlim([x.min(), x.max()])
        axes.set_ylim([y.min(), y.max()])

        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend(loc='upper left')
        plt.show()
 
        j = 1
        while j < 20:
            taylor_function(sin, j, val)
            j += 2
 
    elif sin_or_cos == 'cos':
        x = np.linspace(-7, 7, 256, endpoint = True)
        y = np.cos(x)

        plt.plot(x, y, '-g', label=r'$cos$')

        axes = plt.gca()
        axes.set_xlim([x.min(), x.max()])
        axes.set_ylim([y.min(), y.max()])

        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend(loc='upper left')
        plt.show()
 
        j = 1
        while j < 18:
            taylor_function(cos, j, val)
            j += 2

    elif sin_or_cos == 'e':
        x = np.linspace(-7, 7, 256, endpoint = True)
        y = np.exp(x)

        plt.plot(x, y, '-g', label=r'$e$')

        axes = plt.gca()
        axes.set_xlim([x.min(), x.max()])
        axes.set_ylim([y.min(), y.max()])

        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend(loc ='upper left')
        plt.show()

        j = 1
        while j < 14:
            taylor_function(exp, j, 0)
            j += 1

    else:
        print("enter only sin or cos or e")
        calling()

calling()
